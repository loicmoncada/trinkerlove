const { join } = require("./people");

module.exports = {
    title: function (){
        return `
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
   $$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
   $$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
   $$ |$$ |  \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
   $$ |$$ |      $$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
   $$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
   \\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
   `.yellow
    },

    line: function(title = "="){
        return title.padStart(48, "=").padEnd(96, "=").bgBrightYellow.black
    },

    nb:function(p,f,pa){
        //rentre un tableau (p) un filtre (f) et un parametre (pa)
        let filtre = f(p,pa)
        return filtre.length
        //retourne un nombre

    },

    genre: function(p,g){
         //rentre un tableau (p) un filtre (g)
        return p.filter(i=>i.gender===g);
        //retourne un tableau
    },
    interest: function(p,g){
        //rentre un tableau (p) un filtre (g)
        return p.filter(i=>i.looking_for===g);
        //retourne un tableau
        
    },
    salary: function(p,n){
        //rentre un tableau (p) un filtre (n)
        return p.filter(i=>parseFloat(i.income.slice(1))>n);
        //retourne un tableau
    },
  
    allMovie: function(p,f){
        //rentre un tableau (p) un filtre (f)
        return p.filter(i=>i.pref_movie.includes(f))
        //retourne un tableau
    },
    // ==================niveau 2============================
    
    lNI:function(p,f,pa){
        //rentre un tableau (p) un filtre (f) et un parametre(pa)
        let filtre = f(p,pa)
        let end=[]
        for(let elem of filtre){
            end.push({name:elem.first_name,id:elem.id,age:parseInt(this.quelAge(elem.date_of_birth))})
        }
        return end
        //retourne un tableau
    },
 
   richer:function(p){
    //rentre un tableau (p)
    let cres = 0;
    for(let i of p){
        let j = parseFloat((i.income).slice(1))
        if(cres <j){
            cres = j;
            cresId = i.id
            cresNom =i.last_name
        }
    }
    return {id:cresId,name:cresNom}
    //retourne un objet 
   },
   le_riche:function(p){
    //rentre un tableau (p)
        return this.richer(this.genre(p,'Male'))
        //rentre un objet
   },

   moyenne:function(arr){
    //rentre un tableau (arr)
    sum=0
    for(let i of arr){
        sum +=  i
    }
    return sum/arr.length;
    // retourn un float de la moyenne de l'enssemble des entité dans le tableau arr
   },
   moyen :function(p){
    //rentre un tableau (p)
        return this.moyenne(this.allSalaryArr(p,0));
        // retourne un float
   },

   order:function(p){
    //rentre un tableau (p)
    p.sort(function(a, b) {
        return a - b;
      });
      return p
      //retourne un tableau trié 
   },
   allSalaryArr(p){
    //rentre un tableau (p)
    let end=[]
    for(let i = 0;i<p.length;i++){
        end.push(parseFloat((p[i].income).slice(1)))
    }
    return end
    //retourne un tableau
   },

   mediane:function(p){
    //rentre un tableau (p)
  
    return (this.order(this.allSalaryArr(p))[499]+
    this.order(this.allSalaryArr(p))[500])/2;
    //retourne un float 

   },
   
    latitude:function(p, direc){
        //rentre un tableau (p) et un string(direc)
    if(direc=='n'){
        return p.filter(i=>i.latitude > 0)
    }
    if(direc=='s'){
        return p.filter(i=>i.latitude < 0)
    }else{
        return p.filter(i=>i.latitude == 0)
    }
    //retourne un tableau
   },

   moySouth:function(p){
    //rentre un tableau (p)
    return this.moyenne(this.allSalaryArr(this.latitude(p,'s'),0))
    // retourne un float
   },

   findGPS: function(p,fn,ln){
    //rentre un tableau (p) deux string (ln, fn)
    let wanted =p.filter(i=>i.first_name == fn && i.last_name == ln)
    return {x:wanted[0].latitude, y:wanted[0].longitude,id:wanted[0].id}
    // retourne un objet
   },

   pythagore:function(p1,p2){
    // rentre deux objets de forme {x:<value>,y:<value>}
    if(typeof p1 == typeof {} || typeof p2 == typeof {}){
        let cx = Math.abs(p1['x']-p2['x']);
        let cy = Math.abs(p1['y']-p2['y']);
        let c = {x:cx,y:cy};
        return Math.sqrt((Math.pow(c['x'],2))+(Math.pow(c['y'],2)));
    }else{
        return "Erreur, veuillez rentrer une variable sous forme => a{x:0,y:0}";
    }
    //retourne un float
    
},
allGpsDistances:function(p,fn,ln){
    //rentre un tableau (p) deux string (ln, fn)
    let end=[]
    for(let ele of p){
        end.push({dist:this.pythagore({x:ele.latitude,y:ele.longitude},(this.findGPS(p,fn,ln))),name:ele.first_name,id:ele.id})
    }
    return end
    //retourne un tableau
},
nearest:function(p,fn,ln,n){
    //rentre un tableau (p) deux string (ln, fn) et un int (n)
    let neat = this.allGpsDistances(p,fn,ln)
    this.os(neat,'dist')
    let end=[]
    for(let i= 1;i<n+1;i++){
        end.push({dist:Math.trunc(100*(neat[i].dist)*111.11)/100 +' kM',name:neat[i].name,id:neat[i].id})
    }
    return end
    //retourne un tableau
},
mail:function(p,f){
    //rentre un tableau (p) et un filtre(f )
    return p.filter(i=>i.email.includes(f))
    //retourne un tableau
},

quelAge(str){
    //rentre un string(str) de forme (YYYY-MM-DD)
    date =str.split('-')
    age= 2023-parseInt(date[0])
    jours = 365 -(parseInt(date[2])-(parseInt(date[1])*31))
    return age-jours/365+1
    //retourne un float
},

os:function(tab,by){
    //rentre un tableau (tab) et un string (by)
    tab.sort(function(a, b) {
        return a[by] - b[by];
      });
    return tab
    //retourne un tableau
},
orderByAge: function(p){
    //rentre un tableau (p)
    let end = []
    for(let ele of p){  
        end.push({age:this.quelAge(ele.date_of_birth),name:ele.first_name,id:ele.id,birth:ele.date_of_birth})
    } 
return this.os(end,'age')
//retourne un tableau
},
older:function(p){
    //rentre un tableau (p)
    return (this.orderByAge(p))[999]
    // retourne un objet
},
younger:function(p){
    //rentre un tableau (p)
    return (this.orderByAge(p))[0]
    //retourne un objet
},

allAge:function(p){
    //rentre un tableau (p)
    end=[]
    for(let ele of p){
        end.push(this.quelAge(ele.date_of_birth))
    }
    return end
    // retourne un tableau
},
moyAge:function(p){
    //rentre un tableau (p)
    end=[]
    for(let ele1 of p){
        for(let ele2 of p){
            end.push(Math.abs(ele1-ele2))
        }
    }
    return this.moyenne(end)
    //retourne un float
},

retMA:function(p){
    //rentre un tableau (p)
    return this.moyAge(this.allAge(p))
    //retourne un float
},

allRestult:function(p,argu){
    //rentre un tableau (p) et un string(argu)
    let allM =[]
    let list =[]
    let arr=[]
    
    for(let ele of p){
        allM.push({pref:ele[argu]})
    }
    for(let elem of allM){
        split= elem.pref.split("|")
        for(let spl of split){
            list.push(spl)
        }           
    }
    for(let element of list ){
        if(!(arr.includes(element))){
            arr.push(element)
    }
}
    return arr
    //retourne un tableau
},  
bestOf:function(p,ty,argu){
    //rentre deux tableau (p, ty) et un string(argu)
    let count =[]
    for(let j=0;j<ty.length;j++){
        count.push({nb:0,type:ty[j]})
        for(let i=0;i<p.length;i++){
            if((p[i][argu]).includes(ty[j])){
                count[j].nb+=1
            }
    }
}
    return this.os(count,'nb')
    //retourne un tableaus
},
bestMovie(p,argu){
    //rentre un tableau (p) et un string(argu)
    end=this.bestOf(p,this.allRestult(p,argu),argu)
    return end[end.length-1]
    // retourne un objet
},
movieClass:function(p,argu){
    //rentre un tableau (p) et un string(argu)
    
    return this.bestOf(p,this.allRestult(p,argu),argu)
    //retourne un objet
},
moyennSalary:function(p){
    //rentre un tableau (p)
    end=[]
    for(let ele of p){
        if(ele.gender=='Male'){

            end.push(parseFloat((ele.income).slice(1)))
        }
    }
    return this.moyenne(end)
    //retourne un float
},
ampliGps:function(p,where){
    //rentre un tableau (p) et un string(where)
    end=[]
    const { find } = require('geo-tz')
    for(let elem of p ){
        let ob = this.findGPS(p,elem.first_name,elem.last_name)
        if(find(ob.x,ob.y) == where && parseFloat(elem.income.slice(1)) < this.moyennSalary(p)){
            end.push(parseInt(this.quelAge(elem.date_of_birth)))
        }
    }
    return this.moyenne(end)
    //retourne un float
},
 sameMovies: function(p,arg){
    //rentre un tableau (p) et un string(arg)
    let am = this.allRestult(p,arg)
    end =[]
    for(let ty of am){
        end.push({type:ty,id:''})
        for(let e=0;e<p.length;e++){
            if(ty==p[e][arg]){
                end['id']+=p[e]['id']

            }
        }
    }
    return 
    //retourne rien pour l'instant
},

hForH:function(p){
// let g= this.genre(this.interest(p,'M'),"Male")

},

distReel:function(x){
    //rentre un float
   return Math.asin((100/2)/6371)*6371*2
//    retourne un float
},



match: function(p){
        return "not implemented".red;
    },


}